#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

import addict
import clize
from sigtools.modifiers import annotate
from sigtools.modifiers import autokwoargs

from bookshops.utils import simplecache

from bookshops.utils.baseScraper import BaseScraper
from bookshops.utils.decorators import catch_errors
from bookshops.utils.scraperUtils import is_isbn
from bookshops.utils.scraperUtils import priceFromText
from bookshops.utils.scraperUtils import priceStr2Float
from bookshops.utils.scraperUtils import price_fmt
from bookshops.utils.scraperUtils import print_card
from bookshops.utils.scraperUtils import rmPunctuation


logging.basicConfig(level=logging.ERROR)

# logging.basicConfig(format='%(levelname)s [%(name)s]:%(message)s', level=logging.DEBUG)
logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s]:%(message)s', level=logging.ERROR)
log = logging.getLogger(__name__)


class Scraper(BaseScraper):

    query = ""

    def set_constants(self):
        #: Name of the website
        self.SOURCE_NAME = "abebooks"
        #: Base url of the website
        self.SOURCE_URL_BASE = "https://www.abebooks.com"
        #: Url to which we just have to add url parameters to run the search
        self.SOURCE_URL_SEARCH = "https://www.abebooks.com/servlet/SearchResults?&sts=t&cm_sp=SearchF-_-topnav-_-Results&ds=20&kn="
        #: advanced url (search for isbns)
        # self.SOURCE_URL_ADVANCED_SEARCH = "http://www.librairie-de-paris.fr/listeliv.php?RECHERCHE=appro&LIVREANCIEN=2&MOTS="
        self.SOURCE_URL_ADVANCED_SEARCH = self.SOURCE_URL_SEARCH
        #: the url to search for an isbn.
        self.SOURCE_URL_ISBN_SEARCH = self.SOURCE_URL_SEARCH
        #: Optional suffix to the search url (may help to filter types, i.e. don't show e-books).
        self.URL_END = ""  # search books
        self.TYPE_BOOK = "book"
        #: Query parameter to search for the ean/isbn
        self.ISBN_QPARAM = ""
        #: Query param to search for the publisher (editeur)
        self.PUBLISHER_QPARAM = ""
        #: Number of results to display
        self.NBR_RESULTS_QPARAM = ""
        self.NBR_RESULTS = 30

    def __init__(self, *args, **kwargs):
        """
        """
        self.set_constants()
        super(Scraper, self).__init__(*args, **kwargs)

    def pagination(self):
        """Format the url part to grab the right page.

        Return: a str, the necessary url part to add at the end.
        """
        return ""

    def _product_list(self):
        """
        Return: a list of dom/soup elements holding data of a search result.
        """
        if is_isbn(self.query):
            try:
                # If we got redirected to 1 result page:
                plist = self.soup.find(class_='result-block')
                if not plist:
                    logging.warning('Warning: product list is null, we (apparently) didn\'t find any result')
                    return []
                return [plist]
            except Exception as e:
                logging.error("Error while getting product list. Will return []. Error: {}".format(e))
                return []

        # Normal keyword search.
        try:
            plist = self.soup.find_all(class_='result-item')
            if not plist:
                logging.warning('Warning: product list is null, we (apparently) didn\'t find any result')
                return []

            return plist
        except Exception as e:
            logging.error("Error while getting product list. Will return []. Error: {}".format(e))
            return []

    def _nbr_results(self):
        pass

    @catch_errors
    def _details_url(self, product):
        # if is_isbn(self.query):
        #   # on abebooks, an ISBN or keyword search yields many results.
            # return self.req.url  # we probably got redirected to the book's page.

        details_url = product.find(class_="title").a['href']
        details_url = self.SOURCE_URL_SEARCH + details_url
        return details_url

    @catch_errors
    def _title(self, product):
        title = product.find(class_="title").text.strip()
        return title

    @catch_errors
    def _authors(self, product):
        """
        Return a list of str.
        """
        authors = product.find(class_="author").text.strip()
        if ";" in authors:
            # Murakami, Haruki; Haruki, Murakami
            authors = authors.split(";")
            return [aut.strip() for aut in authors]
        if "-" in authors:
            #  MURAKAMI Haruki - (Hélène Morita)
            authors = authors.split("-")
            return [rmPunctuation(aut.strip()) for aut in authors]

        # However, sometimes authors are separated by comas
        # Valentin, Dieter/Zimmermann, Ralf
        # like a normal name:
        #  Murakami, Haruki
        #  so what can I do in this case?

        return [authors]

    @catch_errors
    def _img(self, product):
        img = product.find(class_="srp-image-holder").img['src']
        return img

    @catch_errors
    def _publisher(self, product):
        """
        Return a list of publishers (strings).
        """
        pub = product.find(class_="opt-publisher")
        if pub:
            pub = pub.text.strip()
            pub = pub.replace(",", "")
            return [pub]
        return []

    @catch_errors
    def _price(self, product):
        """
        The real price (as float), without discounts.
        """
        price = product.find(class_="item-price-group").find(class_="item-price")
        if not price:
            return
        price = priceFromText(price.text.strip())
        price = priceStr2Float(price)
        return price

    @catch_errors
    def _isbn(self, product):
        """
        Return: str
        """
        if is_isbn(self.query):
            return self.query

        node = product.find(class_="isbn").find(class_="pl-md")  # "ISBN 13: 978…"
        if node and ":" in node.text:
            isbn = node.text.split(':')[-1].strip()
            if not is_isbn(isbn):
                logging.warning("Could not find ISBN in this text: {}".format(node.text))

            return isbn.strip()

    @catch_errors
    def _description(self, product):
        # Pas de description dans les résultats par mot-clef.
        return

    @catch_errors
    def _details(self, product):
        # looks deprecated.
        pass

    @catch_errors
    def _date_publication(self, product):
        date_publication = product.find(class_="opt-publish-date").text.strip()
        return date_publication

    @catch_errors
    def _availability(self, product):
        """
        Return: string.
        """
        return

    @catch_errors
    def _dimensions(self, product):
        """
        Épaisseur, hauteur, largeur, poids.
        Return: tuple of numbers.
        """
        if not is_isbn(self.query):
            return [None, None, None, None]

        # return [epaisseur, hauteur, largeur, None]

    @catch_errors
    def _weight(self, product):
        """
        Return: float.
        """
        return

    @catch_errors
    def _nb_pages(self, product):
        return

    @catch_errors
    def _format(self, product):
        """
        Possible formats :pocket, big.

        - return: str or None
        """
        # FMT_POCKET = "Format poche"
        # FMT_LARGE = "Grand format"
        # return res
        pass

    @catch_errors
    def _lang(self, product):
        # We can infer the lang from the ISBN!!
        return

    def search(self, *args, **kwargs):
        """Searches books. Returns a list of books.

        From keywords, fires a query and parses the list of
        results to retrieve the information of each book.

        args: liste de mots, rajoutés dans le champ ?q=
        """
        if self.cached_results is not None:
            log.debug("search: hit cache.")
            # print("-- cache hit for ".format(self.ARGS))
            assert isinstance(self.cached_results, list)
            return self.cached_results, []

        bk_list = []
        stacktraces = []

        product_list = self._product_list()
        self.NBR_RESULTS = len(product_list)

        for product in product_list:
            b = addict.Dict()
            b.authors = self._authors(product)
            b.authors_repr = ", ".join(b.authors) if b.authors else ""
            b.title = self._title(product)
            publishers = self._publisher(product)
            b.publishers = publishers
            b.pubs_repr = ", ".join(publishers) if publishers else ""
            b.search_terms = self.query
            b.data_source = self.SOURCE_NAME
            b.search_url = self.url
            b.date_publication = self._date_publication(product)
            b.details_url = self._details_url(product)  # TODO:
            # b.fmt = self._format(product)
            b.nb_pages = self._nb_pages(product)
            b.price = self._price(product)
            b.price_fmt = price_fmt(b.price, self.currency)
            b.currency = self.currency
            b.img = self._img(product)
            b.summary = self._description(product)
            b.isbn = self._isbn(product)
            b.availability = self._availability(product)
            dimensions = self._dimensions(product)
            if dimensions and len(dimensions) == 4:
                b.thickness, b.height, b.width, b.weight = dimensions
            b.weight = self._weight(product)
            if b.isbn and b.isbn.startswith('9'):
                b.card_type = "book"
            elif b.isbn and (b.isbn.startswith('35') or b.isbn.startswith('37')):
                b.card_type = "cd"
            else:
                b.card_type = "other"
            b.lang = self._lang(product)

            bk_list.append(b.to_dict())

        simplecache.cache_results(self.SOURCE_NAME, self.ARGS, bk_list)
        return bk_list, stacktraces


def postSearch(card, isbn=None):
    """Get a card (dictionnary) with 'details_url'.

    Gets additional data:
    - description

    Check the isbn is valid. If not, return None. But that shouldn't happen !

    We can give the isbn as a keyword-argument (it can happen when we
    import data from a file). In that case, don't look for the isbn.

    Return a new card (dict) complemented with the new attributes.

    """
    return card


@annotate(words=clize.Parameter.REQUIRED)
@autokwoargs()
def main(*words):
    """
    words: keywords to search (or isbn/ean)
    """
    if not words:
        print("Please give keywords as arguments")
        return
    scrap = Scraper(*words)
    bklist, errors = scrap.search()
    print((" Nb results: {}/{}".format(len(bklist), scrap.NBR_RESULTS)))
    bklist = [postSearch(it) for it in bklist]

    list(map(print_card, bklist))


def run():
    exit(clize.run(main))


if __name__ == '__main__':
    clize.run(main)
