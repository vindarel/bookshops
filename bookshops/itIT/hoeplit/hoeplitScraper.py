#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

import addict
import clize
from sigtools.modifiers import annotate
from sigtools.modifiers import autokwoargs

from bookshops.utils import simplecache

from bookshops.utils.baseScraper import BaseScraper
from bookshops.utils.decorators import catch_errors
from bookshops.utils.scraperUtils import is_isbn
from bookshops.utils.scraperUtils import priceFromText
from bookshops.utils.scraperUtils import priceStr2Float
from bookshops.utils.scraperUtils import price_fmt
from bookshops.utils.scraperUtils import print_card


logging.basicConfig(level=logging.ERROR)

# logging.basicConfig(format='%(levelname)s [%(name)s]:%(message)s', level=logging.DEBUG)
logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s]:%(message)s', level=logging.ERROR)
log = logging.getLogger(__name__)

"""
A particularity: an ISBN search redirects to the product page.

We handle both cases (words query and ISBN query) in the same class
and methods (it's the same process, it is not a postSearch).

A regular search doesn't have many data: no publisher, no publication date, no availability. We've seen better! :p

No description, including on the product page.
"""


class Scraper(BaseScraper):

    query = ""

    def set_constants(self):
        #: Internal datasource name.
        self.SOURCE_NAME = "hoeplit"
        #: Base url of the website
        self.SOURCE_URL_BASE = "https://www.hoepli.it"
        #: Url to which we just have to add url parameters to run the search
        # The normal search uses Algolia and JS (Vue) rendering.
        # The advanced search does not and returns HTML.
        self.SOURCE_URL_SEARCH = "https://www.hoepli.it/cerca/libri.aspx?filterCategoryPathROOT=&query="
        #: advanced url (searcf for isbns)
        #: Here: the normal search works for ISBNs, we are redirected to the product page.
        # self.SOURCE_URL_ADVANCED_SEARCH = ""
        #: the url to search for an isbn.
        self.SOURCE_URL_ISBN_SEARCH = self.SOURCE_URL_SEARCH
        #: Optional suffix to the search url (may help to filter types, i.e. don't show e-books).
        self.URL_END = ""
        self.TYPE_BOOK = "book"

        #: Unused:
        #: Query parameter to search for the ean/isbn
        self.ISBN_QPARAM = ""
        #: Query param to search for the publisher (editeur)
        self.PUBLISHER_QPARAM = ""
        #: Number of results to display
        self.NBR_RESULTS_QPARAM = ""
        self.NBR_RESULTS = 28

    def __init__(self, *args, **kwargs):
        """
        """
        self.set_constants()
        super(Scraper, self).__init__(*args, **kwargs)

    def pagination(self):
        """
        Format the url part to grab the right page.

        Return: a str, the necessary url part to add at the end.
        """
        return ""

    def _product_list(self):
        if not self.soup:
            return []

        # ISBN search: we got redirected.
        # we should have self.ISBN_SEARCH_REDIRECTED_TO_PRODUCT_PAGE to True too.
        if self.isbn:
            plist = self.soup.find_all(class_="slbox")
            return plist

        # Now for a words search.
        try:
            node = self.soup.find(id='topTrenta')
            if not node:
                logging.warning('Warning: product list is null, we (apparently) didn\'t find any result')
                return []
            # Book nodes don't have classes. Find images, get the parent node.
            imgs = node.find_all('img')
            plist = []
            for img in imgs:
                try:
                    plist.append(img.parent.parent.parent)
                except Exception as e:
                    logging.info("Italian Hoepli.it scraper: we got an image node, but we can't access its grand-grand-parent.")

            return plist

        except Exception as e:
            logging.error("Error while getting product list. Will return []. Error: {}".format(e))
            return []

    def _nbr_results(self):
        return self.NBR_RESULTS

    @catch_errors
    def _details_url(self, product):
        if self.isbn:
            return self.req.url

        # Nodes don't have custom IDs or classes :( So this is britle.
        details_url = ""
        nodes = product.find_all('a')  # 3 results, one button to discard.
        for node in nodes:
            if node.attrs.get('title'):
                # XXX: filter out class btnAddToCart
                details_url = node.attrs.get('href')
                return self.SOURCE_URL_BASE + details_url

        return details_url

    @catch_errors
    def _title(self, product):
        title = ""
        if self.isbn:
            return self.soup.find("h1").text.strip()

        nodes = product.find_all('a')  # 3 results, one button to discard.
        for node in nodes:
            if node.attrs.get('title'):
                # XXX: filter out class btnAddToCart
                title = node.attrs.get('title')

        return title

    @catch_errors
    def _authors(self, product):
        """Return a list of str.
        """
        if self.isbn:
            authors = self.soup.find(class_="prodotto").find('a').text
        else:
            authors = product.find('span').text  # fragile :S

        # Expecting a list, for multiple authors.
        authors = authors.split(';')
        # authors = [it for it in authors if it != ""]
        authors = [it.strip().capitalize() for it in authors]
        return authors

    @catch_errors
    def _img(self, product):
        if self.isbn:
            # XXL:
            # https://copertine.hoepli.it/hoepli/xxl/978/8818/9788818031294.jpg
            return product.find('img').attrs.get('src')

        img = product.find('img').attrs.get('src')
        if img:
            try:
                img = img.replace('/xl/', '/xxl/')
            except Exception as e:
                logging.info("Italian Hoeplit scraper: enlarging the image doesn't work any more. No /xl/ in URL. It's OK, we have a small one. Error: {}".format(e))
        return img

    @catch_errors
    def _publisher(self, product):
        """
        Return a list of publishers (strings).
        """
        if self.isbn:
            nodes = self.soup.find(class_='dettaglioDati').find_all(class_='altreInfo')
            for node in nodes:
                if "editore" in node.text.lower():
                    return [node.find('a').text]

        # Not present in regular search results.
        return []

    def _price(self, product):
        "The real price (as float), without discounts"
        try:
            if self.isbn:
                # on the product page.
                price = product.find(class_='price').find('span').text.strip()
            else:
                # text query.
                price = product.find('strike').text.strip()

            price = priceFromText(price)
            price = priceStr2Float(price)
            return price
        except Exception as e:
            print(('Erreur getting price {}'.format(e)))

    @catch_errors
    def _isbn(self, product):
        """
        Return: str
        """
        # ISBN search? Easy.
        if self.isbn:
            return self.isbn

        res = product.find("a")
        isbn = res.attrs.get('href').strip()
        isbn = isbn.split('/')[-1]
        isbn = isbn.split('.html')[0]
        if not is_isbn(isbn):
            logging.warning('Italian Hoepli scraper: our ISBN is not an ISBN: {}'.format(isbn))
            return ""
        return isbn

    @catch_errors
    def _description(self, product):
        """To get with postSearch.
        """
        # missing in all cases, even product page.
        pass

    @catch_errors
    def _date_publication(self, product):
        # missing
        return

    @catch_errors
    def _availability(self, product):
        """Return: string.
        """
        # missing
        return

    @catch_errors
    def _format(self, product):
        """
        Possible formats :pocket, big.

        - return: str or None
        """
        # FMT_POCKET = "Format poche"
        # FMT_LARGE = "Grand format"
        pass

    def search(self, *args, **kwargs):
        """
        Searches books. Returns a list of books.

        From keywords, fires a query and parses the list of
        results to retrieve the information of each book.

        For this scraper: also works with an ISBN search.

        args: liste de mots, rajoutés dans le champ ?q=
        """
        if self.cached_results is not None:
            log.debug("search: hit cache.")
            assert isinstance(self.cached_results, list)
            return self.cached_results, []

        bk_list = []
        stacktraces = []

        product_list = self._product_list()
        for product in product_list:
            authors = self._authors(product)
            publishers = self._publisher(product)
            b = addict.Dict()
            #: language
            b.lang = "Italiano"

            b.search_terms = self.query
            b.data_source = self.SOURCE_NAME
            b.search_url = self.url
            b.date_publication = self._date_publication(product)
            b.details_url = self._details_url(product)
            b.fmt = self._format(product)
            b.title = self._title(product)
            b.authors = authors
            b.authors_repr = ", ".join(authors) if authors else ""
            b.price = self._price(product)
            b.price_fmt = price_fmt(b.price, self.currency)
            b.currency = self.currency
            b.publishers = publishers
            b.pubs_repr = ", ".join(publishers)
            b.img = self._img(product)
            b.summary = self._description(product)
            b.isbn = self._isbn(product)
            b.availability = self._availability(product)
            if b.isbn and b.isbn.startswith('97'):
                b.card_type = "book"
            elif b.isbn and (b.isbn.startswith('35') or b.isbn.startswith('37')):
                b.card_type = "cd"
            else:
                b.card_type = "other"

            bk_list.append(b.to_dict())

        simplecache.cache_results(self.SOURCE_NAME, self.ARGS, bk_list)
        return bk_list, stacktraces


def postSearch(card, isbn=None):
    """Get a card (dictionnary) with 'details_url'.

    Gets additional data:
    - description

    Check the isbn is valid. If not, return None. But that shouldn't happen !

    We can give the isbn as a keyword-argument (it can happen when we
    import data from a file). In that case, don't look for the isbn.

    Return a new card (dict) complemented with the new attributes.

    """
    # An ISBN search with the default endpoints redirects to the product page.
    # Looks like we can use the search() method to get a book's details
    return card


@annotate(words=clize.Parameter.REQUIRED)
@autokwoargs()
def main(*words):
    """
    words: keywords to search (or isbn/ean)
    """
    if not words:
        print("Please give keywords as arguments")
        return
    scrap = Scraper(*words)
    bklist, errors = scrap.search()
    print((" Nb results: {}/{}".format(len(bklist), scrap.NBR_RESULTS)))
    bklist = [postSearch(it) for it in bklist]

    list(map(print_card, bklist))


def run():
    exit(clize.run(main))


if __name__ == '__main__':
    clize.run(main)
