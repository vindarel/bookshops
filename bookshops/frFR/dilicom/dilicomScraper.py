#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import logging
import logging.config
import os
import requests
import six
import time

import addict
import clize
import toolz

from sigtools.modifiers import annotate
from sigtools.modifiers import autokwoargs

from bookshops.utils.decorators import catch_errors
from bookshops.utils.scraperUtils import is_isbn
from bookshops.utils.scraperUtils import print_card
from bookshops.utils.scraperUtils import price_fmt

logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s]:%(message)s',
                    level=logging.ERROR)
log = logging.getLogger(__name__)


class Scraper():
    """
    Dilicom SOAP service.

    This is not a scraper, but we keep the class name for automatic inclusion
    into Abelujo.
    """
    currency = '€'
    query = ""
    isbns = []
    words = []

    def set_constants(self):
        #: Name of the website
        self.SOURCE_NAME = "dilicom"

        # self.WSDL = "http://websfel.centprod.com/v2/DemandeFicheProduit?wsdl"
        self.POST_URL = 'http://websfel.centprod.com/v2/DemandeFicheProduit'
        self.DILICOM_USER = os.getenv('DILICOM_USER')
        self.DILICOM_PASSWORD = os.getenv('DILICOM_PASSWORD')
        self.SOURCE_URL_FICHE_PRODUIT = "https://dilicom-prod.centprod.com/catalogue/detail_article_consultation.html?ean={}"

    def __init__(self, *args, **kwargs):
        self.set_constants()
        self.USER_AGENT = "Abelujo"
        self.HEADERS = {'SOAPAction': '""', 'Content-Type': 'text/xml; charset=utf-8'}
        # Get the search terms that are isbn
        if isinstance(args, six.text_type) or isinstance(args, six.string_types):
            args = [args]
        if args:
            self.isbns = list(filter(is_isbn, args))

        # Get the search keywords without isbns
        # unsupported by Dilicom.
        # self.words = list(set(args) - set(self.isbns))

    @catch_errors
    def _details_url(self, product):
        return self.SOURCE_URL_FICHE_PRODUIT.format(product.find('ean13').text)

    @catch_errors
    def _title(self, product):
        title = product.find('libetd') or ""
        if title:
            title = title.text
        return title.title()

    @catch_errors
    def _authors(self, product):
        """Return a list of str.
        """
        auteur = product.find('auteur') or ""
        if auteur:
            auteur = auteur.text.title()
        # TODO: multiple authors
        return [auteur]

    @catch_errors
    def _img(self, product):
        # Not available in FEL à la demande.
        return None

    @catch_errors
    def _publishers(self, product):
        """
        Return a list of publishers (strings).
        """
        it = product.find('edit') or ''
        if it and it.text:
            it = it.text.title()
        return [it]

    @catch_errors
    def _distributor_gln(self, product):
        """
        Return the distributor GLN (string).
        """
        it = product.find('gcddistrib') or ''
        if it and it.text:
            it = it.text.title()
        return it

    def _price(self, product):
        "The real price, without discounts"
        price = product.find('prix') or 0
        if price and price.text:
            price = price.text  # "00013000"
            price = float(price)
            # TODO:
            # - code dispo
            price = price / 1000.0
        return price

    def _price_excl_vat(self, product):
        "The price excluding taxes."
        price = product.find('mtht1') or 0
        if price:
            price = price.text  # "00013000"
            price = float(price)
            price = price / 1000.0
        return price

    def _vat1(self, product):
        "The VAT tax (french TVA)"
        tax = product.find('tva1')
        if tax and tax.text:
            tax = tax.text
            tax = float(tax)
            tax = tax / 100.0
        return tax

    @catch_errors
    def _isbn(self, product):
        """
        Return: str
        """
        ean = product.find('ean13').text
        return ean

    @catch_errors
    def _description(self, product):
        # Not available.
        pass

    @catch_errors
    def _dimensions(self, product):
        """
        Épaisseur, hauteur, largeur, poids.
        Return: tuple of ints.
        """
        res = []
        # All xml nodes are present, but they can be without content.
        for name in ['epaiss', 'haut', 'larg', 'poids']:
            prop = product.find(name) or 0
            if prop and prop.text:
                prop = int(prop.text)
            else:
                prop = 0
            res.append(prop)

        return tuple(res)

    @catch_errors
    def _date_publication(self, product):
        """
        Return a string in format YYYY-MM-DD.
        Parsed later in Abelujo.
        """
        # XXX: dtparu != date publication ou pas ? C'est la seule date anyways.
        date_publication = product.find('dtparu') or ""
        if date_publication:
            date_publication = date_publication.text
            if date_publication and len(date_publication) == 8:
                # Ensure yyy-mm-dd, as string.
                tmp = date_publication
                date_publication = "{}-{}-{}".format(tmp[:4], tmp[4:6], tmp[6:8])
            else:
                date_publication = ""
        return date_publication

    @catch_errors
    def _date_fin_com(self, product):
        """
        Return a string in format YYYYMMDD (no dashes).
        Parsed later in Abelujo.
        Not present for many books obviously.
        """
        date_publication = product.find('dtfincom') or ""
        if date_publication:
            date_publication = date_publication.text
            if date_publication and len(date_publication) == 8:
                # Ensure yyy-mm-dd, as string.
                tmp = date_publication
                date_publication = "{}-{}-{}".format(tmp[:4], tmp[4:6], tmp[6:8])
            else:
                date_publication = ""
        date_fin_com = date_publication  # lazy me
        return date_fin_com

    @catch_errors
    def _date_creation_in_fel(self, product):
        """
        Return a string in format YYYYMMDD (no dashes).
        Parsed later in Abelujo.
        """
        date = product.find('datcre') or ""
        if date:
            date = date.text
            if date and len(date) == 8:
                # Ensure yyy-mm-dd, as string.
                tmp = date
                date = "{}-{}-{}".format(tmp[:4], tmp[4:6], tmp[6:8])
            else:
                date = ""
        return date

    @catch_errors
    def _availability(self, product):
        """
        Return: int.
        cf codes in scraperUtils.
        We don't store this in Abelujo, as it is supposed to change anytime.
        """
        availability = product.find('codedispo') or 0
        if availability:
            availability = int(availability.text)
        return availability

    @catch_errors
    def _presentation_publisher(self, product):
        it = product.find('presedit')
        if it and it.text:
            it = it.text.strip()
            return it

    @catch_errors
    def _presentation_shop(self, product):
        it = product.find('presmag')
        if it and it.text:
            it = it.text.strip()
            return it

    def _availability_fmt(self, code):
        if code == 1:
            return "01 - Disponible"
        elif code == 2:
            return "2 – Pas encore paru"
        elif code == 3:
            return "3 – Réimpression en cours"
        elif code == 4:
            return "04 - Provisoirement non disponible"
        elif code == 5:
            return "5 – Ne sera plus distribué par nous"
        elif code == 6:
            return "06 - Arrêt de commercialisation"
        elif code == 7:
            return "07 - Manque sans date"
        elif code == 8:
            return "8 – A reparaître"
        else:
            return "{}".format(code)

    def _theme(self, product):
        theme = product.find('theme') or None
        if theme and theme.text:
            return theme.text
        return None

    @catch_errors
    def _collection(self, product):
        it = product.find('collec')
        if it and it.text:
            return it.text

    @catch_errors
    def _format(self, product):
        """
        Possible formats :pocket, big.

        - return: str
        """
        # TODO:
        pass

    def bulk_search(self, isbns):
        """
        Search for many isbns, 100 max.
        Do 1 post request.
        Return:
        - tuple list of books (dicts), stacktraces
        """
        if len(isbns) > 100:
            log.error("dilicom's bulk_search takes a list of 100 ISBNs max.")
            return [], ["100 ISBNs max. Use search() directly, it handles that."]

        bk_list = []
        stacktraces = []
        envelope_skeleton = """<?xml version='1.0' encoding='utf-8'?>
<soap-env:Envelope xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/"><soap-env:Body><ns0:demandeFicheProduit xmlns:ns0="http://fel.ws.accelya.com/"><demandeur>{USER}</demandeur><motDePasse>{PASSWORD}</motDePasse>{EANS}<multiple>false</multiple></ns0:demandeFicheProduit></soap-env:Body></soap-env:Envelope>"""
        ean_skeleton = """<ean13s>{EAN}</ean13s>"""

        envelope = envelope_skeleton.replace('{USER}', self.DILICOM_USER)\
                                    .replace('{PASSWORD}', self.DILICOM_PASSWORD)

        EANS = ''
        for isbn in isbns:
            skel = ean_skeleton.replace('{EAN}', isbn)
            EANS = EANS + skel

        envelope = envelope.replace('{EANS}', EANS)

        session = requests.Session()
        TIMEOUT = 2  # seconds
        try:
            req = session.post(
                self.POST_URL,
                data=envelope,
                headers=self.HEADERS,
                timeout=TIMEOUT,
            )
        except requests.Timeout as e:
            logging.info('timeout: the Dilicom request was cancelled because it took too long to answer (more than {} seconds). The request was for {}. Error: {}'.format(TIMEOUT, EANS, e))
            stacktraces.append("Dilicom timeout error. The Dilicom server took too long to answer.")
            return [], stacktraces

        if not req.status_code == 200:
            log.error("POST request to Dilicom responded with a non-success status code: {}".format(req.status_code))

        soup = BeautifulSoup(req.text, 'lxml')
        product_list = soup.find_all('elemreponse')  # tags are lowercase.
        code_execution = soup.find('demandeficheproduitrs').find('codeexecution').text
        diagnostic = soup.find_all('diagnostic')
        if diagnostic:
            diagnostic = diagnostic[0].text
        if code_execution != "OK":
            logging.warning('The SOAP request {} on Dilicom was not OK: {}. Diagnostic: {}'.format(self.query, code_execution, diagnostic))

        # is product found?
        for product in product_list:
            code = product.find('codeexecution').text
            if code != 'OK':
                log.error("Code execution not OK for Dilicom result: {}".format(product))
                stacktraces.append("Dilicom error: {}".format(code))
                continue

            isbn = self._isbn(product)

            diagnostic = product.find('diagnostic')
            if diagnostic:
                if diagnostic.text == 'UNKNOWN_EAN':
                    stacktraces.append("EAN inconnu sur Dilicom {}".format(isbn))
                else:
                    # All of them should be caught by != OK.
                    stacktraces.append("Dilicom error: {}".format(diagnostic))
                continue

            b = addict.Dict()
            authors = self._authors(product)
            publishers = self._publishers(product)
            b.authors = authors
            b.search_terms = self.query
            b.data_source = self.SOURCE_NAME
            b.date_publication = self._date_publication(product)
            b.date_fin_com = self._date_fin_com(product)
            b.date_creation_in_fel = self._date_creation_in_fel(product)
            b.details_url = self._details_url(product)
            b.fmt = self._format(product)
            b.title = self._title(product)
            b.authors_repr = ", ".join(authors) if authors else ""

            # Price, price without tax, tax (VAT).
            b.price = self._price(product)
            b.price_fmt = price_fmt(b.price, self.currency)
            b.price_excl_vat = self._price_excl_vat(product)
            b.price_excl_vat_fmt = price_fmt(b.price_excl_vat, self.currency)
            b.dilicom_price_excl_vat = b.price_excl_vat
            b.dilicom_price_excl_vat_fmt = b.price_excl_vat_fmt
            b.vat1 = self._vat1(product)
            b.currency = self.currency

            b.publishers = publishers
            b.pubs_repr = ", ".join(publishers)
            b.distributor_gln = self._distributor_gln(product)
            b.img = self._img(product)
            b.summary = self._description(product)
            b.isbn = isbn

            # We don't want to set / choose the card_type here: not of our responsability.
            # If we set it again it will overwrite what the user chose.
            b.card_type = None

            b.availability = self._availability(product)
            b.availability_fmt = self._availability_fmt(b.availability)
            b.thickness, b.height, b.width, b.weight = self._dimensions(product)
            b.theme = self._theme(product)
            b.presedit = self._presentation_publisher(product)
            b.presmag = self._presentation_shop(product)
            b.collection = self._collection(product)

            bk_list.append(b.to_dict())

        return bk_list, stacktraces

    def search(self, *args, **kwargs):
        """
        Searches ISBNs, possibly hundreds at once, by batch of 100.

        Usage:

        dilicom_query = dilicomScraper.Scraper()
        bklist, errors = dilicom_query.search("key words or ISBN")

        bulk search:

        dilicom_query = dilicomScraper.Scraper(*isbns)  # or set the .isbns field.
        bklist, errors = dilicom_query.search()


        Returns a tuple: list of books, stacktraces.
        """

        if not self.DILICOM_USER or not self.DILICOM_PASSWORD:
            log.warn("Dilicom: no DILICOM_USER or DILICOM_PASSWORD found. Aborting the search.")
            return [], ["No user and password found for Dilicom connection."]

        # Le FEL à la demande ne permet pas de recherche libre!
        if not self.isbns:
            log.warn("Dilicom's FEL a la demande only wants ISBNs, and none was given. Return.")
            return [], ["Please only search ISBNs on Dilicom."]

        if len(self.isbns) > 100:
            log.debug("Searching for more than 100 ISBNs.")

        isbn_groups = toolz.partition_all(100, self.isbns)

        all_results = []
        all_stacktraces = []
        for isbns in isbn_groups:
            res, stacktraces = self.bulk_search(isbns)
            all_results += res
            all_stacktraces += stacktraces
            time.sleep(0.1)

        return all_results, all_stacktraces


@annotate(words=clize.Parameter.REQUIRED)
@autokwoargs()
def main(*words):
    """
    Search for ISBNs. Dilicom's FEL à la demande doesn't accept free search.
    """
    if not words:
        print("Please give ISBNs as arguments")
        return

    isbns = list(filter(is_isbn, words))
    if not isbns:
        print("Dilicom n'accepte pas la recherche libre par mots-clefs. Veuillez chercher par ISBN(s).")
        exit(0)
    scrap = Scraper(*isbns)
    bklist, alerts = scrap.search()

    [print_card(it, details=True) for it in bklist]
    for it in alerts:
        print(it)


def run():
    exit(clize.run(main))


if __name__ == '__main__':
    clize.run(main)
