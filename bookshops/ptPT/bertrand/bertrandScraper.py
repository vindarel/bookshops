#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#
#
# <2023-12-06 Wed> Broken, will be deleted. DEPRECATION.
#
#
#
#
#

from __future__ import unicode_literals

import logging

import addict
import clize
import requests
from bs4 import BeautifulSoup
from sigtools.modifiers import annotate
from sigtools.modifiers import kwoargs

from bookshops.utils import simplecache

from bookshops.utils.baseScraper import BaseScraper
from bookshops.utils.decorators import catch_errors
from bookshops.utils.scraperUtils import isbn_cleanup
from bookshops.utils.scraperUtils import is_isbn
from bookshops.utils.scraperUtils import priceFromText
from bookshops.utils.scraperUtils import priceStr2Float
from bookshops.utils.scraperUtils import price_fmt
from bookshops.utils.scraperUtils import print_card


logging.basicConfig(level=logging.ERROR)

# logging.basicConfig(format='%(levelname)s [%(name)s]:%(message)s', level=logging.DEBUG)
logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s]:%(message)s', level=logging.ERROR)
log = logging.getLogger(__name__)

"""
Nice results, except: we can't find the books' ISBN in a regular
search. We need a post search, with which we get more data: publication date, format, summary…
"""


class Scraper(BaseScraper):

    query = ""

    def set_constants(self):
        #: Internal datasource name.
        self.SOURCE_NAME = "bertrand"
        #: Base url of the website
        self.SOURCE_URL_BASE = "https://www.bertrand.pt"
        #: Url to which we just have to add url parameters to run the search
        # The normal search uses Algolia and JS (Vue) rendering.
        # The advanced search does not and returns HTML.
        self.SOURCE_URL_SEARCH = "https://www.bertrand.pt/pesquisa/"
        #: advanced url (searcf for isbns)
        #: Here: the normal search works for ISBNs, we are redirected to the product page.
        # self.SOURCE_URL_ADVANCED_SEARCH = ""
        #: the url to search for an isbn.
        self.SOURCE_URL_ISBN_SEARCH = self.SOURCE_URL_SEARCH
        #: Optional suffix to the search url
        #: Here: don't show e-books.
        #: To apply only for a words search (not ISBN search).
        self.URL_END = "/+/+/+/eyJ0aXBfYXJ0X3dlYl9pZCI6eyJpZCI6IjEyMiIsIm5hbWUiOiJMaXZybyJ9fQ"
        self.TYPE_BOOK = "book"

        #: Unused:
        #: Query parameter to search for the ean/isbn
        self.ISBN_QPARAM = ""
        #: Query param to search for the publisher (editeur)
        self.PUBLISHER_QPARAM = ""
        #: Number of results to display
        self.NBR_RESULTS_QPARAM = ""
        self.NBR_RESULTS = 28

    def __init__(self, *args, **kwargs):
        """
        """
        self.set_constants()
        super(Scraper, self).__init__(*args, **kwargs)

    def pagination(self):
        """
        Format the url part to grab the right page.

        Return: a str, the necessary url part to add at the end.
        """
        return ""

    def _product_list(self):
        if not self.soup:
            return []

        # ISBN search: same results page.
        # Words search:
        try:
            plist = self.soup.find_all(class_='product-portlet')
            # ebooks are excluded with the search URL.
            if not plist:
                logging.warning('Warning: product list is null, we (apparently) didn\'t find any result')
                return []
            return plist

        except Exception as e:
            logging.error("Error while getting product list. Will return []. Error: {}".format(e))
            return []

    def _nbr_results(self):
        return self.NBR_RESULTS

    @catch_errors
    def _details_url(self, product):
        details_url = product.find(class_='title-lnk').attrs.get('href')
        return self.SOURCE_URL_BASE + details_url

    @catch_errors
    def _title(self, product):
        title = product.find(class_='title-lnk').text
        return title

    @catch_errors
    def _authors(self, product):
        """Return a list of str.
        """
        authors = product.find(class_='authors').find_all('a')
        authors = [it.text for it in authors]

        # Expecting a list, for multiple authors.
        # authors = authors.split(';')
        # authors = [it for it in authors if it != ""]
        # authors = [it.strip().capitalize() for it in authors]
        return authors

    @catch_errors
    def _img(self, product):
        img = product.find('picture').find('source').attrs.get('data-srcset')
        return img

    @catch_errors
    def _publisher(self, product):
        """
        Return a list of publishers (strings).
        """
        # Not present in search results.
        return []

    def _price(self, product):
        "The real price (as float), without discounts"
        try:
            # regular words query.
            price = product.find(class_='price')
            old_price = price.find(class_='old-price')
            if old_price:
                price = old_price.text
            else:
                price = price.find(class_='active-price').text

            price = priceFromText(price)
            price = priceStr2Float(price)
            return price
        except Exception as e:
            print(('Erreur getting price {}'.format(e)))

    @catch_errors
    def _isbn(self, product):
        """
        Return: str
        """
        # ISBN search? Easy.
        if self.isbn:
            return self.isbn

        # TODO: WARN! No ISBN in keyword search results !! :S
        res = product.find("a")
        isbn = res.attrs.get('href').strip()
        isbn = isbn.split('/')[-1]
        isbn = isbn.split('.html')[0]
        if not is_isbn(isbn):
            logging.warning('Italian Hoepli scraper: our ISBN is not an ISBN: {}'.format(isbn))
            return ""
        return isbn

    @catch_errors
    def _description(self, product):
        """To get with postSearch.
        """
        # Is present on the product page.
        pass

    @catch_errors
    def _date_publication(self, product):
        """
        Return: a string (not a date object).
        """
        # missing, present on the product page.
        return

    @catch_errors
    def _availability(self, product):
        """
        Return: string.
        """
        # missing, present on the product page.
        return

    @catch_errors
    def _format(self, product):
        """
        Possible formats :pocket, big.

        - return: str or None
        """
        # FMT_POCKET = "Format poche"
        # FMT_LARGE = "Grand format"
        pass

    def search(self, *args, **kwargs):
        """
        Searches books. Returns a list of books.

        From keywords, fires a query and parses the list of
        results to retrieve the information of each book.

        For this scraper: also works with an ISBN search.

        args: liste de mots, rajoutés dans le champ ?q=
        """
        if self.cached_results is not None:
            log.debug("search: hit cache.")
            assert isinstance(self.cached_results, list)
            return self.cached_results, []

        bk_list = []
        stacktraces = []

        product_list = self._product_list()
        for product in product_list:
            authors = self._authors(product)
            publishers = self._publisher(product)
            b = addict.Dict()
            #: language
            b.lang = "Português"

            b.search_terms = self.query
            b.data_source = self.SOURCE_NAME
            b.search_url = self.url
            b.date_publication = self._date_publication(product)
            b.details_url = self._details_url(product)
            b.fmt = self._format(product)
            b.title = self._title(product)
            b.authors = authors
            b.authors_repr = ", ".join(authors) if authors else ""
            b.price = self._price(product)
            b.price_fmt = price_fmt(b.price, self.currency)
            b.currency = self.currency
            b.publishers = publishers
            b.pubs_repr = ", ".join(publishers)
            b.img = self._img(product)
            b.summary = self._description(product)
            b.isbn = self._isbn(product)
            b.availability = self._availability(product)
            if b.isbn and b.isbn.startswith('97'):
                b.card_type = "book"
            elif b.isbn and (b.isbn.startswith('35') or b.isbn.startswith('37')):
                b.card_type = "cd"
            else:
                b.card_type = "other"

            bk_list.append(b.to_dict())

        simplecache.cache_results(self.SOURCE_NAME, self.ARGS, bk_list)
        return bk_list, stacktraces


def postSearch(details_url, isbn=None, card={}):
    """
    Get a card (dictionnary) with 'details_url'.

    - details_url: url (str)
    - isbn: isbn (str)
    - card: dict

    Gets additional data:
    - isbn (after a keywords search)
    - publication date
    - etc

    Return a new card (dict) complemented with the new attributes.
    """
    # An ISBN search with the default endpoints redirects to the product page.
    # Looks like we can use the search() method to get a book's details
    url = details_url or card.get('details_url') or card.get('url')
    if not url:
        log.error("postSearch error: url is False ! ({}).".format(url))
        return None

    req = requests.get(url)
    soup = BeautifulSoup(req.text, "lxml")

    #: Summary.
    try:
        summary = soup.find(id='productPageSectionAboutBook-sinopse').text
        if summary:
            log.info("postSearch of {}: we got the summary: {}".format(url, summary))
            card['summary'] = summary.strip()
    except Exception as e:
        log.info("postSearch of {}: could not get the summary: {}".format(url, e))

    details = soup.find(id='productPageSectionDetails').find(class_='data').find_all('span')

    try:
        if not card.get('isbn'):
            isbn = ""
            for detail in details:
                if 'ISBN' in detail.text:
                    try:
                        isbn = detail.find(class_='info').text
                        if isbn:
                            isbn = isbn_cleanup(isbn.strip())
                        card["isbn"] = isbn
                        log.debug("postSearch of {}: we got isbn {}.".format(url, isbn))
                    except Exception as e:
                        # that's a warn.
                        log.warning("postSearch of {}: could not get the ISBN: {}".format(url, e))

        if not card.get('date_publication'):
            val = ""
            for detail in details:
                if 'ano de ed' in detail.text.lower():
                    try:
                        val = detail.find(class_='info').text
                        card["date_publication"] = val
                        log.debug("postSearch of {}: we got date_publication {}.".format(url, val))
                    except Exception as e:
                        # that's only an info.
                        log.info("postSearch of {}: could not get the date_publication: {}".format(url, e))

        if not card.get('pubs_repr'):
            val = ""
            for detail in details:
                if 'editor' in detail.text.lower():
                    try:
                        val = detail.find(class_='info').text
                        card["pubs_repr"] = val
                        card["publisers"] = [val]
                        log.debug("postSearch of {}: we got publisher(s) {}.".format(url, val))
                    except Exception as e:
                        log.info("postSearch of {}: could not get the publisher(s): {}".format(url, e))

        if not card.get('fmt'):
            val = ""
            for detail in details:
                if 'encarderna' in detail.text.lower():
                    try:
                        val = detail.find(class_='info').text
                        card["fmt"] = val
                        log.debug("postSearch of {}: we got fmt {}.".format(url, val))
                    except Exception as e:
                        log.info("postSearch of {}: could not get the fmt: {}".format(url, e))

        # if not card.get('width'):
        #     val = ""
        #     for detail in details:
        #         if 'dimens' in detail.text.lower():  # dimensões
        #             try:
        #                 val = detail.find(class_='info').text
        #                 val = val.split('x')
        #                 card["width"] = val[0] if val else None
        #                 card["height"] = val[1] if val else None
        #                 # xxx: remove "mm" from thickness
        #                 card["thickness"] = val[2] if val else None
        #                 log.debug("postSearch of {}: we got dimensions {}.".format(url, val))
        #             except Exception as e:
        #                 log.info("postSearch of {}: could not get dimensions: {}".format(url, e))

        if not card.get('nb_pages'):
            val = ""
            for detail in details:
                if 'ginas' in detail.text.lower():  # páginas
                    try:
                        val = detail.find(class_='info').text
                        card["nb_pages"] = val
                        log.debug("postSearch of {}: we got nb_pages {}.".format(url, val))
                    except Exception as e:
                        log.info("postSearch of {}: could not get the nb_pages: {}".format(url, e))

    except Exception as e:
        log.debug("postSearch: error while getting postSearch data of {}: {}".format(url, e))

    return card


@annotate(words=clize.Parameter.REQUIRED)
@kwoargs("postsearch", "nb")
def main(postsearch=False, nb=100, *words):
    """
    words: keywords to search (or isbn/ean)
    """
    if not words:
        print("Please give keywords as arguments")
        return
    scrap = Scraper(*words)
    bklist, errors = scrap.search()
    print((" Nb results: {}/{}".format(len(bklist), scrap.NBR_RESULTS)))
    # ISBN is missing from a keywords search :/
    if postsearch:
        if nb:
            bklist = bklist[:int(nb)]
        bklist = [
            postSearch(
                it.get('details_url'),
                isbn=it.get('isbn'),
                card=it,
            )
            for it in bklist]

    list(map(print_card, bklist))


def run():
    exit(clize.run(main))


if __name__ == '__main__':
    clize.run(main)
