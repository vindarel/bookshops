#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import logging
import re
import string as string_mod
import time
import six

import addict
from termcolor import colored

log = logging.getLogger(__name__)

CODES_DISPO = {
    6: "Arret de commercialisation",
    1: "Disponible",  # ?
    0: "disponibilite inconnue",
}

ISBN_ALLOWED_LENGTHS = [13, 10]


class Timer(object):
    """
    Context manager. If not muted, prints its mesure on stdout.

    Usage:

    with Timer("short description"[, silent=True]):
        pass

    - silent: False: prints its mesure (default). True, do nothing.

    """
    def __init__(self, name="", silent=False):
        if not name:
            name = "Timer"
        self.name = name
        self.silent = silent

    def __enter__(self):
        self.start = time.time()

    def __exit__(self, exc_type, exc_val, traceback):
        self.end = time.time()
        if not self.silent:
            print(("{} lasted: {} sec".format(self.name, self.end - self.start)))


def priceFromText(text):
    """
    Extract the price from text with regexp.
    """
    match = re.search('\d+[,\.]?\d*', text)
    price = match.group()
    return price


def priceStr2Float(str):
    """Gets a str, performs basic formatting and returns a Float.

    Formatting: replaces commas with dots.

    arg: string
    returns: Float
    """
    return float(str.replace(",", "."))


def isbn_cleanup(isbn):
    """
    Clean the string and return only digits. (actually, remove all
    punctuation, most of all the dashes).

    "isbn: 978-3-7 …" => "97837…"

    Because a bar code scanner only prints digits, that's the format
    we want in the database.

    - isbn: a str / unicode
    - return: a string, with only [0-9] digits.

    XXX: replace with isbnlib.canonical
    """
    # note: we duplicated this function in models.utils.
    # Added: pattern extraction.
    res = isbn
    if isbn:
        # note: punctuation is just punctuation, not all fancy characters like « or @
        punctuation = set(string_mod.punctuation)
        res = "".join([it for it in isbn if it not in punctuation])

    if not is_isbn(res):
        pattern = re.compile("([0-9]+)")
        re_search = pattern.search(res)
        if (isinstance(res, six.text_type) or isinstance(res, six.string_types)) and \
           re_search.groups():
            res = re_search.group(1)

    return res


def is_isbn(it):
    """Return True is the given string is an ean or an isbn, i.e:

    - is of type str (or unicode). The string must contain only
      alpha-numerical characters.
    - length of 13 or 10

    XXX: look in isbnlib
    """
    # note: method duplicated from models.utils
    res = False
    pattern = re.compile("[0-9]+")
    if (isinstance(it, six.text_type) or isinstance(it, six.string_types)) and \
       len(it) in ISBN_ALLOWED_LENGTHS and \
       pattern.match(it):
        res = True

    return res


def rmPunctuation(it):
    """
    Remove all punctuation from the string.

    return: str
    """
    # https://stackoverflow.com/questions/265960/best-way-to-strip-punctuation-from-a-string-in-python
    # ret = it.translate(None, string.punctuation) # faster, not with unicode
    if not it:
        return it
    exclude = set(string_mod.punctuation)
    st = ''.join(ch for ch in it if ch not in exclude)
    return st


def print_card(card, details=False):
    """
    Pretty output for the console.
    """
    if not card:
        print("card is void. Do nothing.")
        return
    card = addict.Dict(card)
    # COL_WIDTH = 30
    # TRUNCATE = 19
    # currency = card.get('currency', '€')

    print((colored(" " + card.title, "blue")))
    print(" " + card.authors_repr)  # format fails with an utf-8 author.
    print(" distributeur: {}".format(card.distributor_gln))
    print(" publisher: ", end="")
    print(card.pubs_repr)
    try:
        print("\t{} {}".format(card.price_fmt, card.isbn))
    except Exception:
        pass
    # Great formatting guide: https://pyformat.info/ :)
    if details:
        print((u"   Date publication: {}".format(card.date_publication)))
        print(u"   Date creation in FEL: {}".format(card.date_creation_in_fel))
        if card.get(u'availability'):
            print((u"   {}".format(CODES_DISPO.get(card.availability))))
    print(u"  Dimensions (L,l,é): {}x{}x{}".format(card.get('height'), card.get('width'), card.get('thickness')))
    print(u"  Weight: {}".format(card.get('weight')))

    if card.get('summary'):
        print(card.get('summary'))

    if card.get('img'):
        print(card.get('img'))

    print("type is: ", card.card_type)


def price_fmt(price, currency):
    """
    Return: a unicode string, with the price formatted correctly with its currency symbol.

    Exemple: u"10 €" or u"CHF 10"
    """
    try:
        if price is None:
            return price
        if isinstance(price, six.string_types) or isinstance(price, six.text_type):
            if currency and currency.lower() == 'chf':
                return "CHF {}".format(price)
            elif currency:
                if isinstance(currency, six.text_type):
                    return "{} {}".format(price, currency)
                else:
                    return "{} {}".format(price, currency.decode('utf8'))
            return "{} {}".format(price, '€')
        if currency and currency.lower() == 'chf':
            return 'CHF {:.2f}'.format(price)
        else:
            return '{:.2f} €'.format(price)
    except Exception as e:
        log.error("scraper price_fmt error: {}".format(e))
        return price
