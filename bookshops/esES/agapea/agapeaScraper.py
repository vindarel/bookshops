#!/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) Vincent Dardel, <vindarel@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Spanish book scraper.
https://www.agapea.com

A search gives us:

- ISBN (in URL)
- price
- author
- publisher
- cover

An ISBN search redirects to the book's page, with enough information.

"""

import logging

import clize
from sigtools.modifiers import annotate
from sigtools.modifiers import kwoargs

from bookshops.utils.baseScraper import BaseScraper
from bookshops.utils.scraperUtils import isbn_cleanup
from bookshops.utils.scraperUtils import priceFromText
from bookshops.utils.scraperUtils import priceStr2Float
from bookshops.utils.scraperUtils import print_card
from bookshops.utils.decorators import catch_errors

logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s]:%(message)s', level=logging.warn)
log = logging.getLogger(__name__)


class Scraper(BaseScraper):
    """
    """

    def set_constants(self):
        #: Name of the website
        self.SOURCE_NAME = "agapea"
        #: Base url of the website
        self.SOURCE_URL_BASE = "https://www.agapea.com"
        #: Url to which we just have to add url parameters to run the search
        self.SOURCE_URL_SEARCH = "https://www.agapea.com/buscar/buscador.php?texto="
        self.SOURCE_URL_ADVANCED_SEARCH = self.SOURCE_URL_SEARCH
        self.SOURCE_URL_ISBN_SEARCH = self.SOURCE_URL_SEARCH
        #: Optional suffix to the search url (may help to filter types, i.e. don't show e-books).
        self.TYPE_BOOK = "book"
        self.URL_END = ""
        self.ISBN_QPARAM = ""

    query = ""

    def __init__(self, *args, **kwargs):
        """
        """
        self.set_constants()
        super(Scraper, self).__init__(*args, **kwargs)

    def _product_list(self):
        if not self.soup:
            return []
        if self.isbn:
            return self.soup.find_all(class_="book-page")

        items = self.soup.find(class_="resultados")
        items = items.find_all(class_="resumen-mini")
        return items

    @catch_errors
    def _title(self, product):
        if self.isbn:
            title = product.find(class_='datos-libro').find('h1').text
            return title
        return product.find("h4").text.strip().capitalize()

    @catch_errors
    def _details_url(self, product):
        if self.isbn:
            return self.url
        details_url = product.find("h4").find("a")["href"].strip()
        details_url = self.SOURCE_URL_BASE + details_url
        return details_url

    @catch_errors
    def _price(self, product):
        if self.isbn:
            price = product.find(class_="precio").text
        else:
            price = product.find(class_="precio").find(class_="dolar").text

        if price:
            price = priceFromText(price)
            price = priceStr2Float(price)
            return price

    @catch_errors
    def _authors(self, product):
        authors = []
        if self.isbn:
            return [product.find(class_="visible-phone").text]
        authors = product.find(class_="info1").find("ul").find("li").text.strip().capitalize()
        authors = authors.split(";")
        return authors

    @catch_errors
    def _description(self, product):
        """
        No description in the result page.  There is a sumary in the
        details page. See postSearch.
        """
        return ""

    @catch_errors
    def _img(self, product):
        if self.isbn:
            return product.find(class_="img-prev").find("img")["src"]
        img = product.find("figure").find("img")["src"]
        return img

    @catch_errors
    def _publisher(self, product):
        """
        Return: a list of publishers.
        """
        publisher = ""
        if self.isbn:
            elts = product.find(class_="detalles-libro").find_all("tr")
            for elt in elts:
                if "editorial" in elt.text.lower():
                    return elt.find("td").text.split(";")

        elts = product.find(class_="info1").find("ul").find_all("li")
        publisher = elts[2] if elts else None
        if publisher:
            publisher = publisher.text.strip()
        return [publisher]

    @catch_errors
    def _date(self, product):
        # not available in keyword search results.
        if self.isbn:
            elts = product.find(class_="detalles-libro").find_all("tr")
            for elt in elts:
                if "edic" in elt.text.lower():
                    return elt.find("td").find("span").text

        return ""

    @catch_errors
    def _isbn(self, product):
        if self.isbn:
            return self.isbn
        isbn = product.find("figure").find("a")["href"]
        if isbn:
            isbn = isbn.split("/")[-1]
            elts = isbn.split("-")
            for elt in elts:
                if elt.startswith("9") and len(elt) in [10, 13]:
                    return isbn_cleanup(elt)


def postSearch(details_url, isbn=None, card={}):
    """Get the ean/isbn."""
    return card


@annotate(words=clize.Parameter.REQUIRED)
@kwoargs("postsearch", "nb")
def main(postsearch=False, nb=100, *words):
    """
    words: keywords to search (or isbn/ean)
    """
    if not words:
        print("Please give keywords as arguments")
        return
    scrap = Scraper(*words)
    bklist, errors = scrap.search()
    print((" Nb results: {}".format(len(bklist))))
    if postsearch:
        if nb:
            bklist = bklist[:int(nb)]
        bklist = [
            postSearch(
                it.get('details_url'),
                isbn=it.get('isbn'),
                card=it,
            )
            for it in bklist]
    list(map(print_card, bklist))


def run():
    exit(clize.run(main))


if __name__ == '__main__':
    clize.run(main)
