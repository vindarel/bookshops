#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

import addict
import clize
from sigtools.modifiers import annotate
from sigtools.modifiers import kwoargs

from bookshops.utils import simplecache

from bookshops.utils.baseScraper import BaseScraper
from bookshops.utils.decorators import catch_errors
from bookshops.utils.scraperUtils import is_isbn
from bookshops.utils.scraperUtils import isbn_cleanup
from bookshops.utils.scraperUtils import priceFromText
from bookshops.utils.scraperUtils import priceStr2Float
from bookshops.utils.scraperUtils import price_fmt
from bookshops.utils.scraperUtils import print_card


logging.basicConfig(level=logging.ERROR)

# logging.basicConfig(format='%(levelname)s [%(name)s]:%(message)s', level=logging.DEBUG)
logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s]:%(message)s', level=logging.ERROR)
log = logging.getLogger(__name__)


class Scraper(BaseScraper):
    """
    This scraper is curiously very imilar to librairiedeparis.
    """

    query = ""

    def set_constants(self):
        #: Name of the website
        self.SOURCE_NAME = "bol.com"
        #: Base url of the website
        self.SOURCE_URL_BASE = "https://www.bol.com/nl/nl/"
        self.SOURCE_URL_FICHE_PRODUIT = "https://www.bol.com/nl/nl/p/"  # todo
        #: Url to which we just have to add url parameters to run the search
        self.SOURCE_URL_SEARCH = 'https://www.bol.com/nl/nl/s/?searchtext='
        #: advanced url (searcf for isbns)
        # self.SOURCE_URL_ADVANCED_SEARCH = ""
        # The advanced search supports: ISBN, availability (available, to be published, not available), author, publisher…
        # and in the case of ISBN search, it redirects to the book page.
        self.SOURCE_URL_ADVANCED_SEARCH = self.SOURCE_URL_SEARCH
        #: the url to search for an isbn.
        self.SOURCE_URL_ISBN_SEARCH = self.SOURCE_URL_SEARCH
        #: Optional suffix to the search url (may help to filter types, i.e. don't show e-books).
        self.URL_END = ""  # search books
        self.TYPE_BOOK = "book"
        #: Query parameter to search for the ean/isbn
        self.ISBN_QPARAM = ""
        #: Query param to search for the publisher (editeur)
        self.PUBLISHER_QPARAM = ""
        #: Number of results to display
        self.NBR_RESULTS_QPARAM = ""
        self.NBR_RESULTS = 24

    def __init__(self, *args, **kwargs):
        """
        """
        self.set_constants()
        super(Scraper, self).__init__(*args, **kwargs)

    def pagination(self):
        """
        Format the url part to grab the right page.

        Return: a str, the necessary url part to add at the end.
        """
        return ""

    def _product_list(self):
        if not self.soup:
            logging.warning("no html result to process")
            return []
        try:
            plist = self.soup.find(class_="results-area")
            if not plist:
                logging.warning('Warning: product list is null, we (apparently) didn\'t find any result')
                return []

            res = plist.find_all(class_="product-item--row", recursive=True)  # we need recursive for this scraper.
            return res
        except Exception as e:
            logging.error("Error while getting product list. Will return []. Error: {}".format(e))
            return []

    def _nbr_results(self):
        # On this source, nb of results are rightly displayed, but we don't
        # use this anymore.
        pass

    @catch_errors
    def _details_url(self, product):
        pass

    @catch_errors
    def _title(self, product):
        title = product.find(class_='product-title') or ""
        if title:
            title = title.text
        # return title.title()
        return title

    @catch_errors
    def _authors(self, product):
        """
        Return a list of str.
        """
        auteur = product.find(class_='product-creator') or ""
        if auteur:
            auteur = auteur.text.strip().title()
        # TODO: multiple authors
        return [auteur]

    @catch_errors
    def _img(self, product):
        img = product.find(class_='product-image') or ""
        if img:
            img = img.find('img').attrs['src']

        return img

    @catch_errors
    def _publishers(self, product):
        """
        Return a list of publishers (strings).
        """
        # TODO:
        it = product.find('edit') or ''
        if it and it.text:
            it = it.text.title()
        return [it]

    def _price(self, product):
        "The real price, without discounts"
        price = product.find(class_='promo-price') or 0
        if price and price.text:
            price = price.text.strip()  # "22\n\n 99\n" for 22.99
            num, dem = price.split('\n')
            price = int(num) + int(dem)/100.0
        return price

    @catch_errors
    def _isbn(self, product):
        """
        Return: str
        """
        # ISBN search? Easy.
        if self.isbn:
            return self.isbn
        # keyword search:
        raise NotImplementedError


    @catch_errors
    def _description(self, product):
        pass

    @catch_errors
    def _details(self, product):
        # looks deprecated.
        # For the collection?
        pass

    @catch_errors
    def _date_publication(self, product):
        """
        Return a string in format YYYY-MM-DD.
        Parsed later in Abelujo.
        """
        # XXX: dtparu != date publication ou pas ? C'est la seule date anyways.
        date_publication = product.find(class_='product-small-specs') or ""
        if date_publication:
            date_publication = date_publication.find_all('li')[3].text.strip()
        return date_publication

    @catch_errors
    def _availability(self, product):
        """
        Return: string.
        """
        pass

    @catch_errors
    def _format(self, product):
        """
        Possible formats :pocket, big.

        - return: str or None
        """
        pass


    def search(self, *args, **kwargs):
        """
        Searches books. Returns a list of books.

        From keywords, fires a query and parses the list of
        results to retrieve the information of each book.

        args: liste de mots, rajoutés dans le champ ?q=
        """
        if self.cached_results is not None:
            log.debug("search: hit cache.")
            # print("-- cache hit for ".format(self.ARGS))
            assert isinstance(self.cached_results, list)
            return self.cached_results, []

        bk_list = []
        stacktraces = []

        product_list = self._product_list()
        # nbr_results = self._nbr_results()
        for product in product_list:
            authors = self._authors(product)
            publishers = self._publisher(product)
            b = addict.Dict()
            b.search_terms = self.query
            b.data_source = self.SOURCE_NAME
            b.search_url = self.url
            b.date_publication = self._date_publication(product)
            b.details_url = self._details_url(product)
            b.fmt = self._format(product)
            b.title = self._title(product)
            b.authors = authors
            b.authors_repr = ", ".join(authors) if authors else ""
            b.price = self._price(product)
            b.price_fmt = price_fmt(b.price, self.currency)
            b.currency = self.currency
            b.publishers = publishers
            b.pubs_repr = ", ".join(publishers)
            b.img = self._img(product)
            b.summary = self._description(product)
            b.isbn = self._isbn(product)
            b.availability = self._availability(product)
            if b.isbn and b.isbn.startswith('97'):
                b.card_type = "book"
            elif b.isbn and (b.isbn.startswith('35') or b.isbn.startswith('37')):
                b.card_type = "cd"
            else:
                b.card_type = "other"

            bk_list.append(b.to_dict())

        simplecache.cache_results(self.SOURCE_NAME, self.ARGS, bk_list)
        return bk_list, stacktraces


def postSearch(details_url, isbn=None, card={}):
    """
    Get a card (dictionnary) with 'details_url'.

    Get additional data

    Check the isbn is valid. If not, return None. But that shouldn't happen !

    We can give the isbn as a keyword-argument (it can happen when we
    import data from a file). In that case, don't look for the isbn.

    Return a new card (dict) complemented with the new attributes.
    """
    return card


@annotate(words=clize.Parameter.REQUIRED)
@kwoargs("postsearch", "nb")
def main(postsearch=False, nb=100, *words):
    """
    words: keywords to search (or isbn/ean)
    """
    if not words:
        print("Please give keywords as arguments")
        return
    scrap = Scraper(*words)
    bklist, errors = scrap.search()
    print((" Nb results: {}/{}".format(len(bklist), scrap.NBR_RESULTS)))
    # ISBN is missing from a keywords search :/
    if postsearch:
        if nb:
            bklist = bklist[:int(nb)]
        bklist = [
            postSearch(
                it.get('details_url'),
                isbn=it.get('isbn'),
                card=it,
            )
            for it in bklist]

    list(map(print_card, bklist))


def run():
    exit(clize.run(main))


if __name__ == '__main__':
    clize.run(main)
