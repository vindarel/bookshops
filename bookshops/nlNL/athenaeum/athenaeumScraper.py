#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging

import addict
import clize
from sigtools.modifiers import annotate
from sigtools.modifiers import kwoargs

from bookshops.utils import simplecache

from bookshops.utils.baseScraper import BaseScraper
from bookshops.utils.decorators import catch_errors
from bookshops.utils.scraperUtils import is_isbn
from bookshops.utils.scraperUtils import isbn_cleanup
from bookshops.utils.scraperUtils import priceFromText
from bookshops.utils.scraperUtils import priceStr2Float
from bookshops.utils.scraperUtils import price_fmt
from bookshops.utils.scraperUtils import print_card


logging.basicConfig(level=logging.ERROR)

# logging.basicConfig(format='%(levelname)s [%(name)s]:%(message)s', level=logging.DEBUG)
logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s]:%(message)s', level=logging.ERROR)
log = logging.getLogger(__name__)


class Scraper(BaseScraper):
    """
    This scraper is curiously very imilar to librairiedeparis.
    """

    query = ""

    def set_constants(self):
        #: Name of the website
        self.SOURCE_NAME = "athenaeum"
        #: Base url of the website
        self.SOURCE_URL_BASE = "https://www.athenaeum.nl"
        #: Url to which we just have to add url parameters to run the search
        self.SOURCE_URL_SEARCH = "https://www.athenaeum.nl/zoek?b=&q="
        #: advanced url (searcf for isbns)
        # self.SOURCE_URL_ADVANCED_SEARCH = ""
        # The advanced search supports: ISBN, availability (available, to be published, not available), author, publisher…
        # and in the case of ISBN search, it redirects to the book page.
        self.SOURCE_URL_ADVANCED_SEARCH = self.SOURCE_URL_SEARCH
        #: the url to search for an isbn.
        self.SOURCE_URL_ISBN_SEARCH = self.SOURCE_URL_SEARCH
        #: Optional suffix to the search url (may help to filter types, i.e. don't show e-books).
        self.URL_END = ""  # search books
        self.TYPE_BOOK = "book"
        #: Query parameter to search for the ean/isbn
        self.ISBN_QPARAM = ""
        #: Query param to search for the publisher (editeur)
        self.PUBLISHER_QPARAM = ""
        #: Number of results to display
        self.NBR_RESULTS_QPARAM = ""
        self.NBR_RESULTS = 24

    def __init__(self, *args, **kwargs):
        """
        """
        self.set_constants()
        super(Scraper, self).__init__(*args, **kwargs)

    def pagination(self):
        """
        Format the url part to grab the right page.

        Return: a str, the necessary url part to add at the end.
        """
        return ""

    def _product_list(self):
        if not self.soup:
            return []
        try:
            plist = self.soup.find(class_="product-list")
            if not plist:
                logging.warning('Warning: product list is null, we (apparently) didn\'t find any result')
                return []

            # res = plist.find_all(class_="\"product-list__item", recursive=False)  # yes \" O_o
            # if not res:
            res = plist.find_all(class_="product-list__item", recursive=False)  # let's try without \".
            return res
        except Exception as e:
            logging.error("Error while getting product list. Will return []. Error: {}".format(e))
            return []

    def _nbr_results(self):
        # On this source, nb of results are rightly displayed, but we don't
        # use this anymore.
        pass

    @catch_errors
    def _details_url(self, product):
        details_url = product.find(class_="product-title").get("href").strip()
        # other = product.find(class_="product-title").get("href").strip()
        details_url = self.SOURCE_URL_BASE + details_url
        return details_url

    @catch_errors
    def _title(self, product):
        title = product.find(class_='product-title').text.strip()
        return title.capitalize()

    @catch_errors
    def _authors(self, product):
        """
        Return a list of str.
        """
        authors = product.find(class_='item__content__author').text  # xxx many authors ? see list_authors
        authors = authors.split('\n')
        # TODO: multiple authors
        authors = [it for it in authors if it != ""]
        authors = [it.strip().capitalize() for it in authors]
        return authors

    @catch_errors
    def _img(self, product):
        img = product.find('img').get('src')
        # Get a better resolution.
        try:
            img = img.replace('width=120', 'width=480')
        except Exception as e:
            log.info("Could not get a better high cover image: {}".format(e))
        return img

    @catch_errors
    def _publisher(self, product):
        """
        Return a list of publishers (strings).
        """
        return []
        try:
            pub = product.find(class_="editeur")
            if pub:
                # TODO: multiple publishers ?
                return [pub.text.strip()]
        except Exception as e:
            logging.error("Error scraping the publisher(s): {}".format(e))
        return []

    def _price(self, product):
        "The real price (as float), without discounts"
        try:
            price = product.find(class_='newprice').text.strip()
            price = priceFromText(price)
            price = priceStr2Float(price)
            return price
        except Exception as e:
            print(('Erreur getting price {}'.format(e)))

    @catch_errors
    def _isbn(self, product):
        """
        Return: str
        """
        # ISBN search? Easy.
        if self.isbn:
            return self.isbn

        isbn = product.find(class_="item__content__isbn")
        isbn = isbn_cleanup(isbn.text.strip())
        if is_isbn(isbn):
            return isbn
        return ""

    @catch_errors
    def _description(self, product):
        return ""
        # res = product.find(class_="\"item__content__description")
        # if res:
        #     return res

    @catch_errors
    def _details(self, product):
        # looks deprecated.
        # For the collection?
        pass

    @catch_errors
    def _date_publication(self, product):
        return ""
        # date_publication = product.find(class_="MiseEnLigne")
        # if date_publication:
        #     # not available sometimes.
        #     date_publication = date_publication.text.strip()
        # return date_publication

    @catch_errors
    def _availability(self, product):
        """
        Return: string.
        """
        # mmh… not reliable, probably.
        return ""
        # availability = product.find(class_='item_stock')
        # if availability and "en stock" in availability.text.strip().lower():
        #     return 1

    @catch_errors
    def _format(self, product):
        """
        Possible formats :pocket, big.

        - return: str or None
        """
        #
        # we could have "paperback" and "hardcover".
        #
        # FMT_POCKET = "poche"
        # FMT_LARGE = "grand format"
        # fmt = product.find(class_='item_format')
        # fmt = fmt.text.strip()
        # res = None
        # if FMT_LARGE.upper() in fmt.upper():
        #     res = FMT_LARGE
        # elif FMT_POCKET.upper() in fmt.upper():
        #     res = FMT_POCKET

        # return res
        return ""

    def search(self, *args, **kwargs):
        """
        Searches books. Returns a list of books.

        From keywords, fires a query and parses the list of
        results to retrieve the information of each book.

        args: liste de mots, rajoutés dans le champ ?q=
        """
        if self.cached_results is not None:
            log.debug("search: hit cache.")
            # print("-- cache hit for ".format(self.ARGS))
            assert isinstance(self.cached_results, list)
            return self.cached_results, []

        bk_list = []
        stacktraces = []

        product_list = self._product_list()
        # nbr_results = self._nbr_results()
        for product in product_list:
            authors = self._authors(product)
            publishers = self._publisher(product)
            b = addict.Dict()
            b.search_terms = self.query
            b.data_source = self.SOURCE_NAME
            b.search_url = self.url
            b.date_publication = self._date_publication(product)
            b.details_url = self._details_url(product)
            b.fmt = self._format(product)
            b.title = self._title(product)
            b.authors = authors
            b.authors_repr = ", ".join(authors) if authors else ""
            b.price = self._price(product)
            b.price_fmt = price_fmt(b.price, self.currency)
            b.currency = self.currency
            b.publishers = publishers
            b.pubs_repr = ", ".join(publishers)
            b.img = self._img(product)
            b.summary = self._description(product)
            b.isbn = self._isbn(product)
            b.availability = self._availability(product)
            if b.isbn and b.isbn.startswith('97'):
                b.card_type = "book"
            elif b.isbn and (b.isbn.startswith('35') or b.isbn.startswith('37')):
                b.card_type = "cd"
            else:
                b.card_type = "other"

            bk_list.append(b.to_dict())

        simplecache.cache_results(self.SOURCE_NAME, self.ARGS, bk_list)
        return bk_list, stacktraces


def postSearch(details_url, isbn=None, card={}):
    """
    Get a card (dictionnary) with 'details_url'.

    Get additional data

    Check the isbn is valid. If not, return None. But that shouldn't happen !

    We can give the isbn as a keyword-argument (it can happen when we
    import data from a file). In that case, don't look for the isbn.

    Return a new card (dict) complemented with the new attributes.
    """
    return card


@annotate(words=clize.Parameter.REQUIRED)
@kwoargs("postsearch", "nb")
def main(postsearch=False, nb=100, *words):
    """
    words: keywords to search (or isbn/ean)
    """
    if not words:
        print("Please give keywords as arguments")
        return
    scrap = Scraper(*words)
    bklist, errors = scrap.search()
    print((" Nb results: {}/{}".format(len(bklist), scrap.NBR_RESULTS)))
    # ISBN is missing from a keywords search :/
    if postsearch:
        if nb:
            bklist = bklist[:int(nb)]
        bklist = [
            postSearch(
                it.get('details_url'),
                isbn=it.get('isbn'),
                card=it,
            )
            for it in bklist]

    list(map(print_card, bklist))


def run():
    exit(clize.run(main))


if __name__ == '__main__':
    clize.run(main)
