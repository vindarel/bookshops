import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


try:
    import pypandoc
    long_description = pypandoc.convert('README.md', 'rst')
except (IOError, OSError, ImportError):
    long_description = read('README.md')

setup(
    name="bookshops",
    version="0.8",
    packages=find_packages(exclude=["contrib", "doc", "tests"]),

    install_requires=[
        "pip",
        "requests==2.21",
        "beautifulsoup4==4.7.1",
        "soupsieve==1.9.6",
        "isbnlib<4",  # useful tools to manipulate and get isbn
        "lxml==4.6.2",  # parsing
        # "html5lib==1.0b8",  # parsing.
        # WARN: newer version breaks bs4.
        # see https://bugs.launchpad.net/beautifulsoup/+bug/1603299
        "toolz==0.9.0",     # functional utils
        "tabulate==0.8.3",
        "addict==2.2.0",  # fancy dict access
        "termcolor==1.1.0",  # colored print
        "unidecode==1.0.23",  # string clean up
        "distance==0.1.3",  # between two strings
        "sigtools==0.1b2",  # for clize
        "clize==3",  # quick and easy cli args
        "pyyaml==5.4.1",
        "tqdm==4.31",      # progress bar
        "termcolor==1.1.0",  # terminal color
        "six==1.15.0",
    ],

    tests_require=[
        "twine",  # pypi upload
        "pypandoc",  # format the long description
    ],

    package_data={
        # If any package contains *.txt or *.rst files, include them:
        # '': ['*.txt', '*.rst'],
        # And include any *.msg files found in the 'hello' package, too:
        # 'hello': ['*.msg'],
    },

    # metadata for upload to PyPI
    author="vindarel",
    author_email="vindarel@mailz.org",
    description="Get book, dvd and cd information (isbn or search) from real bookstores.",  # noqa: E501
    long_description=long_description,
    license="GNU LGPLv3",
    keywords="bookshop bookstore library book dilicom dvd isbn ean ean13 webscraping",  # noqa: E501
    url="https://gitlab.com/vindarel/bookshops",

    entry_points={
        "console_scripts": [
            "livres = bookshops.frFR.librairiedeparis.librairiedeparisScraper:run",  # noqa: E501

            # not a scraper but a real SOAP web service
            "dilicom = bookshops.frFR.dilicom.dilicomScraper:run",

            # we support Electre in another repository.

            "chezmonlibraire = bookshops.frFR.chezmonlibraire.chezmonlibraireScraper:run",  # noqa: E501
            "bookdepository = bookshops.all.bookdepository.bookdepositoryScraper:run",
            "netherlandbooks = bookshops.nlNL.bol.bolScraper:run",
            "bolcom = bookshops.nlNL.bol.bolScraper:run",
            "boeken = bookshops.nlNL.bol.bolScraper:run",

            "lelivre = bookshops.frFR.lelivre.lelivreScraper:run",
            "frites = bookshops.frFR.filigranes.filigranesScraper:run",
            "libros = bookshops.esES.agapea.agapeaScraper:run",
            "libri = bookshops.itIT.hoeplit.hoeplitScraper:run",
            "livros = bookshops.ptPT.bertrand.bertrandScraper:run",
            "bucher = bookshops.deDE.buchlentner.buchlentnerScraper:run",
            "abebooks = bookshops.all.abebooks.abebooksScraper:run",
            "bookfinder = bookshops.all.bookfinder.bookfinderScraper:run",
            "discogs = bookshops.all.discogs.discogsScraper:run",
            "movies = bookshops.all.momox.momox:run",
        ],
    },

    classifiers=[
        "Environment :: Web Environment",
        "Environment :: Console",
        "Environment :: Plugins",
        "Intended Audience :: Developers",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",  # noqa: E501
        "Topic :: Utilities",
        "Programming Language :: Python :: 3",
        "Topic :: Internet",
        "Topic :: Software Development :: Libraries",
    ],

)
