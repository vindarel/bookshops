# Bibliographic search of BOOKS, CDs and DVDs

This library was initially to search for bibliographic data of books,
and it was expanded for **DVDs** and **CDs**. We can search with
**keywords**, with the **isbn** (so than we can use barcode scanners),
with some advanced search, and we have pagination.

We get the data from existing websites. We scrape:

- for French books, we support official data sources:
  - [**Dilicom**](https://dilicom-prod.centprod.com/)'s profesional provider. See more below and on doc/dilicom.md.
  - note: we support **Electre** in Abelujo.
  - http://www.librairie-de-paris.fr See [its doc](doc/frenchscraper.md)
  - https://www.chez-mon-libraire.fr
- for Switzerland: [lelivre.ch](https://www.lelivre.ch)
- for Spain: Agapea.com (new in Feb, 2023, replace Casa del Libro)
- for Italy: https://www.hoepli.it (new in 2023)
- for Portugal: ~~bertrand.pt~~ use Bookfinder, only for an ISBN search (for barcode scanners).
- for the Netherlands: [bol.com](https://www.bol.com/nl/nl/), ISBN searches only (new in Jan, 2025)
  * before that: https://www.athenaeum.nl (new in 2023, deprecated in 2025) as well as https://www.naibooksellers.nl (new in April, 2023, deprecated in 2024)
- for Germany: http://www.buchlentner.de
- for more international data:
  - Bookfinder.com: new and second hand books, finds a lot of books.
  - AbeBooks: second hand books, worldwilde.
  - (deprecated) https://www.bookdepository.com closed in April, 2023.
- for DVDs: https://www.momox-shop.fr
- and for CDs: https://www.discogs.com (may need more testing)


We retrieve: the title and authors, the price, the isbn, the publisher(s), the cover,... when we can, also the dimensions, the weight, the availability…

This library is used in
[Abelujo](https://gitlab.com/vindarel/abelujo/), a free software for
bookshops.

![](cli-search.png)

# Install

Install from pypi:

    pip install bookshops

# Use

## Command line

You can try this lib on the command line with the following commands:
- `livres`: french books
- `chezmonlibraire`: french books, other source.
- `lelivre`: swiss books
- `frites`: belgium books
- `boeken`: netherlands (athenaeum)
- `dilicom`: Dilicom search (only searches ISBNs, no free search)
- `libros`: spanish books
- `libri`: italian books
- `livros`: portuguese books
- `bucher`: german books
- `bookdepository`: more books in english, deutsh, and more.
- `discogs`: CDs
- `movies`: DVDs

For example:

    livres antigone

or

    livres 9782918059363

and you get the above screenshot.

**Options**: (this may vary according to the scrapers, check them with `-h`)
- `-i` or `--isbn` to ensure to get all the isbn. The command line
  tool won't get them by default if they need to be fetched with
  another http request for each book. That depends on the websites.
- for some scrapers, use `--postsearch` to get more data:
  - Portuguese: get the ISBN (if not an ISBN search, of course), the publisher, the summary, the publication date, the book format.

## Settings

The Dilicom interface requires you to set two environment variables:

    export DILICOM_USER="300xxx"
    export DILICOM_PASSWORD="xyz"

In addition, you can set this third one, that allows you to view a
book's product page on Dilicom's website (within your account). You
find it on the url of your account. For example, when I am visiting this
book page:

    https://dilicom-prod.centprod.com/catalogue/detail_article_consultation.html?ean=9782840550877&emet=3010xxxxx0100

I set the environment variable like so:

    export DILICOM_EMET="3010xxxxx0100"

In Abelujo, this sets the "details_url" Card slot accordingly and you
can click on the "source" link when viewing a book's page.

## As a library

But most of all, from within your program:

    from bookshops.frFR.librairiedeparis.librairiedeparisScraper import Scraper as frenchScraper

    scraper = frenchScraper("search keywords")
    cards = scraper.search()
    # we get a list of dictionnaries with the title, the authors, etc.

## Caching

Results are cached in memory for about 1 day (except Dilicom results,
in purpose). It allows long-running software based on this library
(e.g., Abelujo) to feel more dynamic in certain cases.

## Advanced search

Work in progress.

You can search ``ed:agone`` to search for a specific publisher.

## Pagination

We do pagination:

    scraper = frenchScraper("search keywords", page=2)


# Why not… ?

## Why not Amazon ?

Amazon kills the book industry and its employees.  But moreover, we
can add value to our results. We can link to a good and independent
bookshop from within our application, we could command books from it,
we could say if it has exemplaries in stock or not, etc.

Technically speaking, the Amazon API web service can be too limiting
and not appropriate. One must register to Amazon Product Advertising
and to AWS, making it more difficult for deployment or independant
users, and it changes way more often than our resailers' websites.

## Why not Google books ?

It has very few data.

## Why not the BNF (Bibliothèque Nationale de France) ?

Because, for bookshops, we need recent books (they enter the BNF
database after a few months), and the price.


# Develop and test

See http://dev.abelujo.cc/webscraping.html

Development mode:

    pip install -e .

Now you can edit the project and run the development version like the
lib is meant to be run, i.e. with the `entry_points`: `livres`,
`libros`, etc.

doc: https://python-packaging-user-guide.readthedocs.org/en/latest/distributing/#working-in-development-mode

How to find a good scraping source:

- try a regular keywords search: can we find the books' ISBN?
  - if not, we will need a post search request.
- try getting the results with `wget`. If the results are obfuscated, try with Python (and a custom User Agent).
  - inspect the HTML manually, see how it looks like with a simple local server (python -m SimpleHTTPServer or equivalent).
- try a regular search with an ISBN. Do we get a regular search results page with one element, are we redirected to the book's own page? Do we have enough data? (publication date, publisher…)
- is there an "advanced search" page that yields consistent results?


# Bugs and shortcomings

Note: the Dilicom interface is not concerned by these limitations.

This is webscraping, so it doesn't go without pitfalls:

- the site can go down. It happened already.
- the site can change, it which case we would have to change our
  sraper too. To catch this early we run automatic tests every
  week. The actual website didn't change in 3 years.


# Changelog

## DEV

- added French source: chez-mon-libraire.fr
- added NL source: naibooksellers. From an ISBN search, we can get: price, cover image, author(s) string, publisher (not everytime).
- added Abebooks source: results are good, price inevitably isn't the official one (for the few countries that have unique books price).

## 0.12 (2023/02)

- added Italian and Portuguese scrapers.
- changed the Spanish scraper: from Casa del Libro (broken, the site dramatically changed) to Agapea.com.

## 0.10, 0.11, 0.12 (2022/09)

- added 2s timeouts for all scrapers, including the Dilicom connector. A slow datasource impacts Abelujo. There was no timeout by default in the requests library :S
- new scraper: bookdepository (english, Netherlands)

Specific to scrapers:

- Dilicom: an ISBN starting with 84 is set as a product of type map.
- Dilicom: add missing availability codes.
- Dilicom: minor incompatibly change: return the date_publication as string, not date object.
- the french scraper gets a better resolution image.

NOTE: Abelujo was ported to Python 3, this library seems to be working, it still needs tests.

## 0.9 (2020/09)

- Belgium scraper (because Banque du Livre's "FEL à la demande" by FTP doesn't work as advertised: the serveur takes 3 to 5 minutes to process our input, and even looses files in the process. We are awaiting the deploy "FEL à la demande" by WEB SERVICE)
  - available fields: title, authors, price, ISBN.

Get more Dilicom fields:

- price excluding the VAT
- VAT 1
- présentation éditeur (relié broché disque etc)
- collection

## 0.8 more Dilicom

Get more Dilicom fields:

- distributor GLN
- theme
- fix thickness in some cases

In Abelujo, you can import a list of distributors with their known GLN and address.

## 0.7

- multiple ISBN search for Dilicom (by batches of one hundred).

## 0.6

- results are cached again. Simply in memory for about 1 day.

## 0.5

- added a Swiss interface.
- added support to fetch and print prices in another currency.

## 0.4

- added Dilicom interface. It only provides search of ISBN(s), it doesn't provide free and advanced search.

## 0.3.1

- added search of DVDs
- updated french scrapers (first time needed in four years).

## 0.2.2

- remove deprecated import from ods/csv feature. Might do a simpler one in the future.

## 0.2.1

- german scraper: search by isbn

## 0.2.0

- German scraper
- multiprocessing for the german scraper (from 15 to 9s) (see [issue #1](https://gitlab.com/vindarel/bookshops/issues/1))
- `--isbn` option for it

## 0.1.x

- french, spanish scrapers
- command line tool

# Licence

LGPLv3
